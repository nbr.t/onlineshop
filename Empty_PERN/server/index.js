require('dotenv').config()
const express = require('express')
const sequelize = require('./db')
const models = require('./models/models')
const cors = require('cors')
const fileUpload = require('express-fileupload')
const PORT = process.env.PORT || 7000
const router = require('./routes/index')
const errorHandler = require('./middleware/ErrorHandingMiddleware')
const path = require('path')


const app = express()
app.use(cors())
app.use(express.json())
app.use(express.static(path.resolve(__dirname, 'static')))
app.use(fileUpload({}))
app.use('/api', router)

//обработка ошибок, последний middleware
app.use(errorHandler)

/*app.get('/', (reg,res) => {
    res.status(200).json({message: 'WORKING!'})
})*/

const start = async () => {
   try {
       await sequelize.authenticate()
       await sequelize.sync()
       app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
   }  catch(e) {
       console.log(e)
   }
    //app.listen(PORT, () => console.log(`Server started on port ${PORT}`))

}

start()

